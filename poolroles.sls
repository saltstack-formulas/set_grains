{% set dnsdomainname = "domain.tld" %}

{% set salt_hostname = grains['id'] %}
{% set salt_versioninfo = grains['saltversioninfo'] %}
{% set force = False %}
{% if salt_versioninfo[0] > 2015 %}
 {% set force = True %}
{% elif salt_versioninfo[0] == 2015 and salt_versioninfo[1] > 8 %}
 {% set force = True %}
{% elif salt_versioninfo[0] == 2015 and salt_versioninfo[1] == 8 and salt_versioninfo[2] > 2 %}
 {% set force = True %}
{% endif %}

#{{ salt_hostname|pprint }}

###############################################
# initialize the pool grain from the hostname #
###############################################

# remove it before appending it again
removing init pool grains:
  grains.absent:
    - name: pool
    - destructive: True
    - force: True

set pool:
  grains.list_present:
    - name: pool
    - value:
 {%- if salt["network.in_subnet"]('10.1.252.0/24') or 'compute' in salt_hostname or 'mbbnode' in salt_hostname or '_mbb' in salt_hostname or '_clmbb' in salt_hostname %}
      - mbb_cluster
 {% endif -%}
 {%- if 'mbb.domain.tld' in salt_hostname %}
      - mbb_isem
 {% endif -%}
 {%- if salt["network.in_subnet"]('192.168.212.0/24') or '212' in salt_hostname %}
      - isem
      - isem212
 {% endif -%}
 {%- if salt["network.in_subnet"]('10.255.62.0/24') %}
      - storagepool
 {% endif -%}
 {%- if salt["network.in_subnet"]('192.168.64.0/22') %}
      - isem
      - isem64
 {% endif -%}

###############################################
# initialize the role grain from the hostname #
###############################################

{% set allroles = salt['pillar.get']('roles', {}) %}

# remove it before appending it again
deleting all the following role:
  grains.list_absent:
    - name: roles
    - value:
{% for roles, rolesinfos in allroles.items() %}
      - {{ roles }}
{% endfor %}

# initialize the 'roles' grain
# force is avalaible since salt 2015.8.2

init master role grain:
  grains.present:
    - name: roles
    - value:
      - salt_master
{% if force == True %}
    - force: True
{% endif %}
    - onlyif:
      - ls /etc/salt/master

init minion role grain:
  grains.present:
    - name: roles
    - value:
      - salt_minion
{% if force == True %}
    - force: True
{% endif %}
    - unless:
      - ls /etc/salt/master

{% for roles, rolesinfos in allroles.items() %}
 {% set parentloop = loop %}
 {% if 'exact_salt_hostname' in rolesinfos %}
  {% for exact_salt_hostname in rolesinfos['exact_salt_hostname'] %}
   {% set fullname = exact_salt_hostname ~ "." ~ dnsdomainname %}
   {% if exact_salt_hostname == salt_hostname or fullname == salt_hostname %}
set exact_roles {{ parentloop.index }}_{{ loop.index }}:
  grains.append:
    - name: roles
    - value:
      - {{ roles }}
   {% endif %}
  {% endfor %}
 {% endif %}

 {% if 'minimal_salt_hostname' in rolesinfos %}
  {% for minimal_salt_hostname in rolesinfos['minimal_salt_hostname'] %}
   {% if minimal_salt_hostname in salt_hostname %}
    {% if not 'except' in rolesinfos or salt_hostname not in rolesinfos['except'] %}

set minimal_roles {{ parentloop.index }}_{{ loop.index }}:
  grains.append:
    - name: roles
    - value:
      - {{ roles }}

    {% endif %}
   {% endif %}
  {% endfor %}
 {% endif %}

{% endfor %}

# adding a role based on a running service
{%- if salt["service.available"]('prometheus-node-exporter') or salt["ps.pgrep"]('node_exporter') %}
adding node_exporter prometheus roles if service found:
  grains.append:
    - name: roles
    - value:
      - 'node_exporter'
{% endif %}


delete init role:
  grains.list_absent:
    - name: roles
    - value:
      - salt_minion
