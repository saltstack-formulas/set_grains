# Set Grains

This SaltStack formula defines additional grains to apply on the host based on its subnets and its Salt ID (`minion id` or `hostname` otherwise).

Caution: 
> Version 3001 has a bug with an impossible deletion when the grain value is a list : https://github.com/saltstack/salt/issues/57718
> The value must then be re-assigned to this grain by forcing it `salt '$minion' grains.set pool foo force=True`), then delete it again with `salt '$minion' grains.delkey pool`.

Usage:
```bash
salt '*' state.sls set_grains
```

```bash
# to delete a grain value in a list
salt 'target' grains.remove roles <value>
# to delete a whole key, use:
salt 'target' grains.delval key
```

Edit `poolroles.sls` and `pillar.roles.example` to fit your needs.

## Parent

`Parent` is used and may be uncomment in `init.sls` to specify a `parent` grain. It allows us to create a diagram with machine hardware dependencies.

For example, you can imagine having a _proxmox_ server with some virtual machines or containers.

  - The _proxmox_ server will have a pillar value of machine type: `machine_type: host`,
  - A virtual machine will have a pillar value of machine type: `machine_type: vm`,
  - A container will have a pillar value of machine type: `machine_type: container`.

Those 3 type of machines will also have an IP address.

VM and containers will also have a `url_mgmt` pillar value.
This value is used to check if a IP address is already present in host machines.

See `hosts212.example` for more informations on how to do this.
