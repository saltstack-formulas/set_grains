{% set fulldict = {} %}
{% set hosts212 = salt['pillar.get']('machines212', {}) %}
# mixing pillar machines dicts from many subnets
#{#% set hosts197 = salt['pillar.get']('machines197', {}) %#}
{% do fulldict.update(hosts212) %}
#{#% do fulldict.update(hosts197) %#}

{% for host, hostinfo in fulldict.items() %}
 {% if 'SaltHostname' in hostinfo %}
  {% if hostinfo['SaltHostname'] == grains['id'] %}
   {% if 'machine_type' in hostinfo %}
    {% if hostinfo['machine_type'] == 'vm' or hostinfo['machine_type'] == 'container' %}
     {% if 'url_mgmt' in hostinfo %}
assign pxmaster parent for {{ host }}:
  grains.present:
    - name: parent
      {% if '210' in hostinfo['url_mgmt'] %}
    - value: "px210"
      {% elif '211' in hostinfo['url_mgmt'] %}
    - value: "px211"
      {% elif '212' in hostinfo['url_mgmt'] %}
    - value: "px212"
      {% else %}
    - value : "Unknown. Please edit hosts212.sls pillar file to add a known url_mgmt key."
      {% endif %}
     {% endif %}
    {% endif %}
   {% endif %}
  {% endif %}
 {% endif %}
{% endfor %}
